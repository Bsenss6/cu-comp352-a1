#!/bin/bash

# Short script to generate the input files and run the java program

for i in {1..6}
do
    if [ -d res/out$i ]
    then
        rm res/out$i/*
    else
        mkdir res/out$i
    fi

    # Generate input files
    shuf res/names.txt -n $((10**i)) > res/names_$i
    ./date-gen res/names_$i > res/input_$i.csv

    # Run java
    java -cp out -Xss100m comp352.aa.a1.Main <<< $i > /dev/null

    # Move output files
    mv res/out_* res/out$i
done

# Clean-up res directory
rm res/names_* res/input_*
#rm -r res/out*

