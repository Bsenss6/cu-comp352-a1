package comp352.aa.a1;

/**
 * Simple class to contain two elements.
 *
 * <p>Can be use as a way to return multiple values of different types.</p>
 */
public class Tuple<A, B> {

    private final A a;
    private final B b;

    /**
     * Constructs a Tuple containing the given elements.
     *
     * @param a the first element of the tuple
     * @param b the second element of the tuple
     */
    public Tuple(A a, B b) {
        this.a = a;
        this.b = b;
    }

    /**
     * Returns the first element of this Tuple.
     * @return The first element of this Tuple.
     */
    public A getA() {
        return a;
    }

    /**
     * Returns the second element of this Tuple.
     * @return The second element of this Tuple.
     */
    public B getB() {
        return b;
    }
}
