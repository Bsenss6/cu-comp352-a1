package comp352.aa.a1;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;

/**
 * Arranges participants for a clinic test based on their date of birth.
 *
 * <p>Given two lists containing the names and the dates of birth of the
 * participants of a clinic test, arranges the lists in the following way.</p>
 *
 * <ul>
 * <li>At index 0 is the oldest (senior) participant.</li>
 * <li>After are the senior participants in decreasing age.</li>
 * <li>After the youngest senior is the oldest junior participant.</li>
 * <li>After are the junior participants in increasing age.</li>
 * <li>At last index is the youngest participant.</li>
 * </ul>
 *
 * <p>A person is considered senior if they are 65 years of age or older.</p>
 */
public class Main {

    public static void main(String[] args) {
        try {
	    run();
        } catch (IOException e) {
            System.err.printf("An I/O error occured. Terminating program.");
            System.exit(1);
        }
    }

    private static void run() throws IOException {

	/* PREPARE BENCHMARKS */
	
	// Read files
	File inputFile = FileParser.promptInputFile();
        Tuple<String[], Date[]> tuple =	FileParser.initializeArrays(inputFile);

	// Allocate arrays
        String[] pName = tuple.getA();
        Date[] pDOB = tuple.getB();
	int nParticipants = pName.length;
	int nSeniors;

	// Benchmark time variables
	long timeStart,
	    timeEnd;
        long timeElapsedRearrange,
	    timeElapsedSeniors,
	    timeElapsedJuniors,
	    timeElapsedAll;

	// Keep reference to STD_OUT
	PrintStream stdout = System.out;

	
	/* RUN FUNCTIONS */

	// Rearrange Participants
	
	timeStart = System.currentTimeMillis();
	nSeniors = Functions.rearrangeParticipants(
		pName, pDOB, nParticipants);
	timeEnd = System.currentTimeMillis();
	
	timeElapsedRearrange = timeEnd - timeStart;

	
	// Display Seniors Increasing Order
	
	try (PrintStream stdoutSeniors =
	     new PrintStream(
		 new BufferedOutputStream(
                     new FileOutputStream("res/out_seniors.txt"))))
	{
	    System.setOut(stdoutSeniors);
	    
	    timeStart = System.currentTimeMillis();
	    Functions.displaySeniorsIncreasingOrder(
	            pName,
		    pDOB,
		    nSeniors);
	    timeEnd = System.currentTimeMillis();
	    
	    timeElapsedSeniors = timeEnd - timeStart;
	}

	
	// Display Non-Seniors Increasing Order
	
	try (PrintStream stdoutJuniors =
	     new PrintStream(
		 new BufferedOutputStream(
                     new FileOutputStream("res/out_junior.txt"))))
	{
	    System.setOut(stdoutJuniors);
	    
	    timeStart = System.currentTimeMillis();
	    Functions.displayNonSeniorsIncreasingOrder(
		    pName,
		    pDOB,
		    nParticipants - nSeniors,
		    nParticipants);
	    timeEnd = System.currentTimeMillis();
	    
	    timeElapsedJuniors = timeEnd - timeStart;
	}

	
	// Display Increasing Order
	
	try (PrintStream stdoutAll =
	     new PrintStream(
		 new BufferedOutputStream(
                     new FileOutputStream("res/out_all.txt"))))
	{
	    System.setOut(stdoutAll);
	    
	    timeStart = System.currentTimeMillis();
	    Functions.displayIncreasingOrder(
		       pName,
		       pDOB,
		       nSeniors,
		       nParticipants);
	    timeEnd = System.currentTimeMillis();
	    
	    timeElapsedAll = timeEnd - timeStart;
	}


	/* LOG STATS */
	try (PrintWriter statsFile =
	     new PrintWriter(
		 new FileOutputStream("res/stats.txt", true)))
	{
	    // Stats File
	    statsFile.println("Size: " + nParticipants);
	    statsFile.println("Sort Participants (ms): " + timeElapsedRearrange);
	    statsFile.println("Display Seniors (ms):   " + timeElapsedSeniors);
	    statsFile.println("Display Juniors (ms):   " + timeElapsedJuniors);
	    statsFile.println("Display All (ms):       " + timeElapsedAll);
	    statsFile.println();

	    // Std Out
	    System.setOut(stdout);
	    System.out.println("Size: " + nParticipants);
	    System.out.println("Sort Participants (ms): " + timeElapsedRearrange);
	    System.out.println("Display Seniors (ms):   " + timeElapsedSeniors);
	    System.out.println("Display Juniors (ms):   " + timeElapsedJuniors);
	    System.out.println("Display All (ms):       " + timeElapsedAll);
	    System.out.println();
	}
    }
}
