package comp352.aa.a1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Arranges participants for a clinic test based on their date of birth.
 *
 * <p>Given two lists containing the names and the dates of birth of the
 * participants of a clinic test, arranges the lists in the following way.</p>
 *
 * <ul>
 * <li>At index 0 is the oldest (senior) participant.</li>
 * <li>After are the senior participants in decreasing age.</li>
 * <li>After the youngest senior is the oldest junior participant.</li>
 * <li>After are the junior participants in increasing age.</li>
 * <li>At last index is the youngest participant.</li>
 * </ul>
 *
 * <p>A person is considered senior if they are 65 years of age or older.</p>
 */
public class Functions {

    private static final DateFormat DATE_FORMAT
	    = new SimpleDateFormat("yyyy-MM-dd");
    private static final Date TODAY;
    public static final Date SENIOR_THRESHOLD;
    static {
        Calendar calendar = Calendar.getInstance();
        TODAY = calendar.getTime();

        calendar.add(Calendar.YEAR, -65);
        SENIOR_THRESHOLD = calendar.getTime();
    }

    /**
     * Arranges the given arrays 
     *
     * <p>Recursive function takes as input two arrays -- <tt>pName</tt> and
     * <tt>pDOB</tt> -- and the number of participants and returns the number
     * of senior participants, in addition to arranging the arrays as specified
     * in {@link Functions}.</p>
     * 
     * <p>Implemented using the merge sort, providing a worst-case time
     * complexity of <tt>O(n log n)</tt>. The arrays are first divided
     * recursively into two halves, until the input is of size 1. An array of
     * size 1 is sorted. The sorted halves are then merged together into a
     * bigger sorted array.<br> In this implementation, the merging is also done
     * recursively, using the method {@link mergeParticipants}.</p>
     *
     * <p>This implementation allocates two new sub-arrays at each call,
     * effectively providing a space complexity of <tt>O(n log n)</tt>.</p> 
     * @param pName array containing the name of the participants
     * @param pDOB array containing the date of birth of the participants
     * @param nParticipants the number of participants in the given arrays.
     * @throws IllegalArgumentException if <tt>nParticipants</tt> does not
     * represent the size of both arrays.
     * @return The number of senior participants in the given arrays.
     */
    /*
     * This method provides input validation and returns early on empty
     * arrays. If inputs are valid and the arrays are not empty, redirects
     * to rearrangeParticipantsUnchecked for actual implementation.
     */
    public static int rearrangeParticipants(
            String[] pName, Date[] pDOB, int nParticipants)
    {
        // Input validation
        if (pName.length != nParticipants)
            throw new IllegalArgumentException(
                    "nParticipants should be size of pName");

        if (pDOB.length != nParticipants)
            throw new IllegalArgumentException(
                    "nParticipants should be size of pDOB");

        // Early return
        if (nParticipants == 0)
            return 0;

        return rearrangeParticipantsUnchecked(pName, pDOB, nParticipants);
    }

    /**
     * Recursive function implementing the logic for rearrangeParticipants.
     *
     * <p>The input validation and early return are done by the checked
     * version. This method implements the logic in a recursive algorithm.</p>
     *
     * @param pName array containing the name of the participants
     * @param pDOB array containing the date of birth of the participants
     * @param nParticipants the number of participants in the given arrays.
     * @return The number of senior participants in the given arrays.
     */
    private static int rearrangeParticipantsUnchecked(
            String[] pName, Date[] pDOB, int nParticipants)
    {
        // Base case
        if (nParticipants == 1) {
            if (isSenior(pDOB[0])) {
                return 1;
            } else {
                return 0;
            }
        }

        // Declaring new variables
        int n1 = nParticipants / 2;
        int n2 = nParticipants - n1; // different of n1 if nParticipants is odd
        String[] pName1 = Arrays.copyOfRange(pName, 0, n1);
        String[] pName2 = Arrays.copyOfRange(pName, n1, nParticipants);
        Date[] pDOB1 = Arrays.copyOfRange(pDOB, 0, n1);
        Date[] pDOB2 = Arrays.copyOfRange(pDOB, n1, nParticipants);

        // Recursive divide call
        int nSenior = rearrangeParticipantsUnchecked(pName1, pDOB1, n1)
            + rearrangeParticipantsUnchecked(pName2, pDOB2, n2);

        // Merging sorted halves
        mergeParticipants(pName, pName1, pName2, pDOB, pDOB1, pDOB2,
                nParticipants - 1, n1 - 1, n2 - 1); // See method doc for `-1'

        // Return value
        return nSenior;
    }

    /**
     * Recursively merge two sorted subarrays into a sorted one.
     *
     * <p>Tail recursive helper method for rearrangeParticipants</p>
     *
     * <p>To save three arguments, instead of merging from bottom to top
     * (first to last) and passing parameters for length and index (<tt>n</tt>
     * and <tt>i</tt>), the sub-arrays are merged from top to bottom, and the
     * size arguments also acts as the index, decreasing until 0.</p>
     *
     * <p>The arguments <tt>i</tt>, <tt>i1</tt> and <tt>i2</tt> are used as
     * the sizes <tt>nParticipants[1|2]</tt>, but minus 1. This avoids
     * computing <tt>nParticipants - 1</tt> each time an element of the array
     * is indexed (<tt>array[i]</tt> instead of <tt>array[nParticipants -
     * 1]</tt>).</p>
     *
     * @param pName merged array of names
     * @param pName1 left sub-array of names
     * @param pName2 right sub-array of names
     * @param pDOB merged array of dates
     * @param pDOB1 left sub-array of dates
     * @param pDOB2 right sub-array of dates
     * @param i <tt>length - 1</tt> of merged array, used as last index
     * @param i1 <tt>length - 1</tt> of left sub-array, used as last index
     * @param i2 <tt>length - 1</tt> of right sub-array, used as last index
     */
    private static void mergeParticipants(
            String[] pName, String[] pName1, String[] pName2,
            Date[] pDOB, Date[] pDOB1, Date[] pDOB2,
            int i, int i1, int i2)
    {
        if (i >= 0) { // Base case
            if (i2 < 0) // array2 fully merged
            {
                pName[i] = pName1[i1];
                pDOB[i--] = pDOB1[i1--];
            }
            else if (i1 < 0) // array1 fully merged
            {
                pName[i] = pName2[i2];
                pDOB[i--] = pDOB2[i2--];
            }
            else // both arrays still merging
            {
                int order = compare(pDOB1[i1], pDOB2[i2]);

                if (order <= 0) // date1 < date2
                {
                    pName[i] = pName2[i2];
                    pDOB[i--] = pDOB2[i2--];
                }
                if (order >= 0) // date1 > date2
                {
                    pName[i] = pName1[i1];
                    pDOB[i--] = pDOB1[i1--];
                }
            }

            // Recursive call
            mergeParticipants(
                    pName, pName1, pName2, pDOB, pDOB1, pDOB2, i, i1, i2);
        }
    }

    /**
     * Displays the seniors sorted by their age.
     *
     * <p>Recursive function that takes as input the two arrays <tt>pName</tt>
     * and <tt>pDOB</tt> and the number of senior participants and display the
     * name and DOB of senior participants in an increasing order based on
     * their age.</p>
     *
     * <p>It is assumed that the given arrays are already sorted by using 
     * {@link rearrangeParticipants}.</p>
     *
     * <p>Since this implementation works on an already sorted array, it prints
     * all the seniors from the beginning of the array by using a single
     * recursive call, effectively providing a time complexity of O(n).</p>
     *
     * @param pName array containing the name of the participants
     * @param pDOB array containing the date of birth of the participants
     * @param nSeniors the number of senior participants in the given arrays.
     */
    public static void displaySeniorsIncreasingOrder(
            String[] pName, Date[] pDOB, int nSeniors)
    {
        // Base case
        if (nSeniors > 0) {

            nSeniors--; // nSeniors - 1 often used as index

            // Print senior
            System.out.printf("%s:\t%s%n",
                pName[nSeniors],
                DATE_FORMAT.format(pDOB[nSeniors]));

            // Tail recursive call
            displaySeniorsIncreasingOrder(pName, pDOB, nSeniors);
        }	    
    }

    /**
     * Displays the non-seniors sorted by their age.
     *
     * <p>Recursive function that takes as input the two arrays <tt>pName</tt>
     * and <tt>pDOB</tt>, the number of non-senior participants and the total
     * number of participants, and display the name and DOB of non-senior
     * participants in an increasing order based on their age.</p>
     *
     * <p>It is assumed that the given arrays are already sorted by using 
     * {@link rearrangeParticipants}.</p>
     *
     * <p>Since this implementation works on an already sorted array, it prints
     * all the juniors from the beginning of the array by using a single
     * recursive call, effectively providing a time complexity of O(n).</p>
     *
     * @param pName array containing the name of the participants
     * @param pDOB array containing the date of birth of the participants
     * @param nNonSeniors the number of non-senior participants in the given arrays.
     */
    public static void displayNonSeniorsIncreasingOrder(
            String[] pName, Date[] pDOB, int nNonSeniors, int nParticipants)
    {
        // Base case
        if (nNonSeniors != 0) {

            int idx = nParticipants - nNonSeniors;

            // Print non-senior
            System.out.printf("%s:\t%s%n",
                    pName[idx],
                    DATE_FORMAT.format(pDOB[idx]));

            // Tail recursive call
            displayNonSeniorsIncreasingOrder(
                    pName, pDOB, nNonSeniors - 1, nParticipants);
        }
    }

    /**
     * Displays the participants' information sorted by their age.
     *
     * <p>Function that takes as input the two arrays <tt>pName</tt>
     * and <tt>pDOB</tt>, the number of senior participants and the total number
     * of participants and displays the name and DOB of all participants in an
     * increasing order based on their age.</p>
     *
     * @param pName array containing the name of the participants
     * @param pDOB array containing the date of birth of the participants
     * @param nSeniors the number of senior participants in the given arrays.
     * @param nParticipants the total number of participants in given arrays.
     */
    public static void displayIncreasingOrder(
	    String[] pName, Date[] pDOB, int nSeniors, int nParticipants)
    {
        // Display Non-Seniors
        for (int i = nSeniors; i < nParticipants; i++) {
            System.out.printf("%s:\t%s%n",
                    pName[i],
                    DATE_FORMAT.format(pDOB[i]));
        }

        // Display Seniors
        for (int i = nSeniors - 1; i >= 0; i--) {
            System.out.printf("%s:\t%s%n",
                    pName[i],
                    DATE_FORMAT.format(pDOB[i]));
        }
    }

    /**
     * Returns whether the given date is that of a senior participant.
     *
     * <p>A participant is a senior if his birth date is equal to or anterior
     * to {@link Functions#SENIOR_THRESHOLD}.</p>
     *
     * @param date the date of the participant
     * @return <tt>false</tt> if the given date is posterior to the limit
     * date; <tt>true</tt> otherwise.
     */
    private static boolean isSenior(Date date) {
        return !date.after(SENIOR_THRESHOLD); // Before or equal
    }

    /**
     * Provides a custom total ordering over the dates.
     *
     * <p>This function returns an <tt>int</tt> representing the order of the
     * two inputs. The returned value is negative if the first input is ordered
     * before the second, a <tt>0</tt> if both inputs are equal, and a
     * positive if the first is ordered after the second input.</p>
     *
     * <p>The ordering is defined as follows:
     * <ul>
     * <li>The seniors are ordered before the non-seniors;</li>
     * <li>The seniors are sorted chronologically;</li>
     * <li>The non-seniors are sorted in reversed chronological order;</li>
     * </ul>
     * Where a senior is a date that is at least 65 years old.<br>
     * Or alternatively: <tt>old senior &lt; young senior &lt; young
     * non-senior &lt; old non-senior</tt>.</p>
     *
     * @param a first date to be compared
     * @param b second date to be compared
     * @return The value <tt>0</tt> if both arguments are equal; a value less
     * than 0 if <tt>a</tt> is ordered before <tt>b</tt>; and a value greater
     * than 0 if <tt>a</tt> is ordered after <tt>b</tt>.
     */
    private static int compare(Date a, Date b) {
        if (isSenior(a) || isSenior(b))
            return a.compareTo(b);
        else
            return b.compareTo(a);
    }

}

