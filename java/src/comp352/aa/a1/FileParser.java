package comp352.aa.a1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Class used to prompt the user for an input file and to parse that file.
 */
public class FileParser {

    /**
     * This is the directory in which the program will first look to find
     * potential input files to offer to the user.
     */
    private static final String DEFAULT_DIRECTORY = "res";

    /**
     * Prompts the user for an input file path.
     *
     * <p>First look for <tt>.csv</tt> files inside the <tt>res/</tt>
     * directory. If some files are found, the user is prompted to select one
     * of these files. Otherwise, the user types the path of the desired file.
     * Loops until a valid file is found.</p>
     *
     * @return The file selected by the user.
     */
    public static File promptInputFile() {
        Scanner scanner = new Scanner(System.in);

        File resDir = new File(DEFAULT_DIRECTORY);

        if (resDir.isDirectory()) {

            // Get .CSV files in res/ directory
            File[] resContents = resDir.listFiles(
                    f -> f.getName().matches(".+\\.csv"));

            if (resContents.length != 0) {

		Arrays.sort(resContents); // Sort files by name

                // Prompt user for a file
                System.out.println("Select an input file.");
                System.out.println("[0]: other file");
                for (int i = 0; i < resContents.length; i++) {
                    System.out.printf("[%d]: %s%n", i+1, resContents[i]);
                }

                int input;
                while (true) {
                    System.out.print("> ");

                    try {
                        input = Integer.parseInt(scanner.nextLine());
                    } catch (NumberFormatException e) {
                        // Loop again if invalid
                        continue;
                    }

                    if (input > 0 && input <= resContents.length) {
                        return resContents[input - 1]; // returns selected file
                    } else if (input == 0) {
                        break; // type file name
                    }
                    // Loop again if invalid
                } // user selected `other file'
            } // res empty
        } // no res

        System.out.println("Enter a file path.");

        String input;
        File typedFile;
        while (true) {
            // Prompt user
            System.out.print("> ");
            input = scanner.nextLine();
            typedFile = new File(input);
            
            // Return file if valid
            if (typedFile.isFile()) {
                return typedFile;
            }

            System.out.println("File not found.");
        }
    }

    /**
     * Initialize names and DOBs from a CSV file.
     *
     * <p>Reads the given input file and parse each line into the name and DOB
     * of a participant. The file should be in CSV format, where the first
     * field is the name and the second contains the date, in a format
     * specified in {@link java.text.DateFormat}. Returns a {@link Tuple}
     * containing an array with the name of the participants and an array with
     * their date of birth.</p>
     *
     * <p>When attempting to parse an invalid line or a line containing an
     * invalid date format, a message is printed to STDERR and the line is
     * ignored.</p>
     *
     * @param inputFile path to the file to be parsed
     * @return A Tuple with both arrays.
     */
    public static Tuple<String[], Date[]> initializeArrays(File inputFile)
        throws IOException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        List<String> pName = new ArrayList<>();
        List<Date> pDate = new ArrayList<>();

        try (BufferedReader file = new BufferedReader(
                new FileReader(inputFile)))
        {
            String line;
            String[] split;
            String name;
            Date date;

            while ((line = file.readLine()) != null) {

                split = line.split(",");

                if (split.length != 2) {
                    System.err.printf(
                            "Parsing file: ignored invalid line format: `%s'.%n",
                            line);
                    continue;
                }

                name = split[0];

                try {
                    date = df.parse(split[1]);
                } catch (ParseException e) {
                    System.err.printf(
                            "Parsing file: ignored invalid date format: `%s'.%n",
                            split[1]);
                    continue;
                }

                pName.add(name);
                pDate.add(date);
            }
        }

        return new Tuple<>(
            pName.toArray(new String[pName.size()]),
            pDate.toArray(new Date[pDate.size()]));
    }
}
