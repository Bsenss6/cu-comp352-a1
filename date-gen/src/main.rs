use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use rand::distributions::{Distribution, Uniform};
use rand::rngs::ThreadRng;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    let names_file = &args[1];

    let f = File::open(names_file)?;
    let reader = BufReader::new(f);

    reader.lines()
        .filter_map(|name| name.ok())
        .zip(Dates::new())
        .for_each(|(name, date)| println!("{},{}", name, date));

    Ok(())
}

struct Dates {
    rng: ThreadRng,
    years: Uniform<u16>,
    months: Uniform<u16>,
    days: Uniform<u16>,
}

impl Dates {
    fn new() -> Self {
        Self {
            rng: rand::thread_rng(),
            years: Uniform::from(1920..=2021),
            months: Uniform::from(1..=12),
            days: Uniform::from(1..=28),
        }
    }
}

impl Iterator for Dates {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        Some(format!("{}-{:02}-{:02}",
                     self.years.sample(&mut self.rng),
                     self.months.sample(&mut self.rng),
                     self.days.sample(&mut self.rng)))
    }
}
