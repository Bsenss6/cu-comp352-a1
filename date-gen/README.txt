This short program is used to quickly generate the files used by the java
program. It takes as input the name of a file. For each line of this file, a
random date will be appended, separated by a comma.
